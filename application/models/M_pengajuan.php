<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengajuan extends CI_Model {
	
	public function get($id = null) 
	{
		$this->db->from('pengajuan');
		if($id != null){
			$this->db->where('id_pengajuan', $id); 
		}
		$query = $this->db->get();
		return $query;
	}

	public function getCeklis()
    {
        $this->db->from('ceklis');
		$query = $this->db->get();
		return $query;
	}

	public function add($post)
	{
		$params = [
			'judul'				=> $post['judul'],
			'tanggal_pengajuan' => $post['tanggal_pengajuan'],
			'proposal' 	 		=> $post['proposal'],
			'deskripsi' 		=> empty($post['deskripsi']) ? null : $post['deskripsi'],
		];
		$this->db->insert('pengajuan',$params);	
		$id_pengajuan = $this->db->insert_id();

		$count_ceklis = count($this->input->post('id_ceklis'));
    	for($x = 0; $x < $count_ceklis; $x++) {
		    if(isset($this->input->post('ceklis_pemohon')[$x]))
		        {
		            $ceklis_pemohon = 1;
		        }
		    else
		    	{
		            $ceklis_pemohon = 0;
		    	}

    		$ceklis = array(
    			'id_pengajuan' => $id_pengajuan,
    			'id_ceklis' => $this->input->post('id_ceklis')[$x],
    			'ceklis_pemohon' =>  $ceklis_pemohon,
    		);
    		$this->db->insert('detail_pengajuan', $ceklis);
    	}
	}

	public function del($id)
	{
		$this->db->where('id_pengajuan', $id);
		$this->db->delete('pengajuan');
		$this->db->where('id_pengajuan', $id);
		$this->db->delete('detail_pengajuan');
	}
}
