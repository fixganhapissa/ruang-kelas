<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {
	
	// fungsi untuk menampilkan data semua user 
	public function get($id_user = null) 
	{
		$this->db->from('user');
		if($id_user != null){
			$this->db->where('id_user', $id_user); //menampilkan user sesuai parameter where

		}
		$query = $this->db->get();
		return $query;
	}

	public function add($post)
	{
		$params['nama'] = $post['nama'];
		$params['no_telp'] = $post['telepon'] != ""? $post['telepon'] : null;
		$params['email'] = $post['email'];
		$params['username'] = $post['username'];
		$params['password'] = md5($post['password']);
		$params['level'] = $post['level'];

		$this->db->insert('user',$params);
	}

	public function edit($post)
	{
		$params['nama'] = $post['nama'];
		$params['no_telp'] = $post['telepon'] != ""? $post['telepon'] : null;
		$params['email'] = $post['email'];
		$params['username'] = $post['username'];
		if (!empty($post['password'])) {
			$params['password'] = md5($post['password']);
		}
		$params['level'] = $post['level'];

		$this->db->where('id_user', $post['id_user']);
		$this->db->update('user',$params);
	}

	public function del($id)
	{
		$this->db->where('id_user', $id);
		$this->db->delete('user');
	}




}
