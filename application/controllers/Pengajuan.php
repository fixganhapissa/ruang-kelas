<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Controller {

	// fungsi yang di panggil pertamakali __construct
	public function __construct()
	{	
		parent::__construct();
		belumlogin();
		$this->load->model('M_pengajuan');
		$this->load->library('form_validation');
	}

	public function index()
	{	
		$data['row'] = $this->M_pengajuan->get();
		$this->template->load('template','pengajuan/pengajuan',$data);	
	}

	public function add()
	{
		// Pengajuan
		$pengajuan = new stdClass();
		$pengajuan->id = null;
		$pengajuan->judul = null;
		$pengajuan->tanggal_pengajuan = null;
		$pengajuan->proposal = null;
		$pengajuan->deskripsi = null;
		$data = array(
			'page'=> 'add',
			'row' => $pengajuan
			);

        $data['row'] = $this->M_pengajuan->getCeklis();
        $this->template->load('template','pengajuan/add',$data);

	}

	public function process()
	{
		$post = $this->input->post(NULL,TRUE);
		if (isset($_POST['add'])) {
			$config['upload_path'] 	= './proposal/';
			$config['allowed_types'] = 'pdf';
			$config['max_size'] 	= 2024;
			$config['file_name'] 	= 'Proposal-'.date('ymd').'-'.substr(md5(rand()),0,10);
			$this->load->library('upload', $config);
    		$this->upload->initialize($config);

			if (@$_FILES['proposal']['name'] != null) {
				if ($this->upload->do_upload('proposal')) {
					$post['proposal'] = $this->upload->data('file_name');
					$this->M_pengajuan->add($post);
					if ($this->db->affected_rows()>0) {
						$this->session->set_flashdata('success','Data berhasil disimpan');
					}
					redirect('pengajuan');
				} else {
					$error = $this->upload->display_errors();
					$this->session->set_flashdata('error', $error);
					var_dump($error);
					// redirect('pengajuan/add');
				}
			} else{
				$post['proposal'] = null;
				$this->M_pengajuan->add($post);
				if ($this->db->affected_rows()>0) {
					$this->session->set_flashdata('success','Data berhasil disimpan');
				}
				redirect('pengajuan');
			}

		} else if (isset($_POST['edit'])){
			$this->M_pengajuan->edit($post);
		}
	}

	public function update($id){

	}

	public function del($id){

		$item = $this->M_pengajuan->get($id)->row();
		if ($item->proposal != null) {
			$target_file = './proposal/'.$item->proposal;
			unlink($target_file);
		}

		$this->M_pengajuan->del($id);

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success','Data berhasil dihapus');
		}
		redirect('pengajuan');
	}

}