<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('user') ?>">User</a></li>
        <li class="breadcrumb-item active">Add</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User<small></small></h1>
    <!-- end page-header -->
    
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">Add</h4>
        </div>
        <!-- end panel-heading -->
        <div class="panel-body">
            <form action="" method="post">
                <div class="form-group row m-b-15 <?php echo form_error('nama') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Nama</label>
                    <div class="col-md-9">
                        <input class="form-control m-b-5" type="text" name="nama" placeholder="Masukkan Nama" value="<?php echo set_value('nama') ?>" >
                        <?php echo form_error('nama') ?>
                    </div>
                </div>
                <div class="form-group row m-b-15 <?php echo form_error('telepon') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Telepon</label>
                    <div class="col-md-9">
                        <input class="form-control m-b-5" type="text" name="telepon" placeholder="Masukkan Telepon" value="<?php echo set_value('telepon') ?>" >
                        <?php echo form_error('telepon') ?>
                    </div>
                </div>

                <div class="form-group row m-b-15 <?php echo form_error('email') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Email</label>
                    <div class="col-md-9">
                        <input class="form-control m-b-5" type="email" name="email" placeholder="Masukkan Email" value="<?php echo set_value('email') ?>" >
                        <?php echo form_error('email') ?>
                    </div>
                </div>
                <div class="form-group row m-b-15 <?php echo form_error('username') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Username</label>
                    <div class="col-md-9">
                        <input class="form-control m-b-5" type="text" name="username" placeholder="Masukkan Username" value="<?php echo set_value('username') ?>" >
                        <?php echo form_error('username') ?>
                    </div>
                </div>

                <div class="form-group row m-b-15 <?php echo form_error('password') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Password</label>
                    <div class="col-md-9">
                        <input class="form-control m-b-5" type="password" name="password" placeholder="Masukkan Password" value="<?php echo set_value('password') ?>" >
                        <?php echo form_error('password') ?>
                    </div>
                </div>
                <div class="form-group row m-b-15 <?php echo form_error('passconf') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Konfirmasi Password</label>
                    <div class="col-md-9">
                        <input class="form-control m-b-5" type="password" name="passconf" placeholder="Masukkan Password" value="<?php echo set_value('passconf') ?>" >
                        <?php echo form_error('passconf') ?>
                    </div>
                </div>

                <div class="form-group row m-b-15 <?php echo form_error('passconf') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Level</label>
                    <div class="col-md-9">
                        <select class="form-control selectpicker" name="level" data-live-search="true"  <?php echo set_value('level') ?>>
                            <option selected="selected" value=""> - Pilih Level - </option>
                            <option value="1" <?php set_value('level') == 1 ? "selected" : null ?>>Admin</option>
                            <option value="2" <?php set_value('level') == 2 ? "selected" : null ?>>Sekolah</option>
                            <option value="3" <?php set_value('level') == 3 ? "selected" : null ?>>Verifikator Berkas</option>
                            <option value="4" <?php set_value('level') == 4 ? "selected" : null ?>>Verifikator Lapangan</option>
                        </select>
                        <?php echo form_error('level') ?>
                    </div>
                </div>

                <div class="panel-footer text-right">
                    <a href="<?php echo base_url('user'); ?>" class="btn btn-white btn-sm">Batal</a>
                    <button type="submit" class="btn btn-primary btn-sm m-l-5">Simpan</button>
                </div>

            </form>
        </div>
        <!-- end panel-body -->
    </div>
    <!-- end panel -->
</div>
<!-- end #content -->

