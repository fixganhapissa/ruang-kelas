<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('user') ?>">Pengajuan</a></li>
        <li class="breadcrumb-item active">Add</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Pengajuan<small></small></h1>
    <!-- end page-header -->
    
    <!-- begin panel -->
    <div class="panel panel-inverse">
        <!-- begin panel-heading -->
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">Form Pengajuan</h4>
        </div>
        <!-- end panel-heading -->
        <div class="panel-body">
            <?php echo form_open_multipart('pengajuan/process') ?>
                <div class="form-group row m-b-15 <?php echo form_error('judul') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Judul Pengajuan</label>
                    <div class="col-md-9">
                        <input class="form-control m-b-5" type="text" name="judul" placeholder="" value="<?php echo set_value('judul') ?>" >
                        <?php echo form_error('judul') ?>
                    </div>
                </div>
                <div class="form-group row m-b-15 <?php echo form_error('tanggal_pengajuan') ? 'has-error' : null ?>">
                    <label class="col-md-3 col-form-label">Tanggal Pengajuan</label>
                    <div class="col-md-9">
                        <input type="text" name="tanggal_pengajuan" class="form-control" id="from-datepicker" value="<?php echo set_value('tanggal_pengajuan') ?>" >
                        <!-- <input type="text" class="form-control" id="from-datepicker"> -->
                        <?php echo form_error('judul') ?>
                    </div>
                </div>
                <div class="form-group row m-b-15 <?php echo form_error('proposal') ? 'has-error' : null ?>">
                    <label class="col-md-3 col-form-label">Proposal Pengajuan</label>
                    <div class="col-md-9">
                        <input type="file" name="proposal" class="form-control" >
                        <?php echo form_error('proposal') ?>
                    </div>
                </div>

                <div class="form-group row m-b-15 <?php echo form_error('deskripsi') ? 'has-error' : null ?>">
                    <label class="col-form-label col-md-3" >Deskripsi</label>
                    <div class="col-md-9">
                        <textarea class="form-control" rows="5" name="deskripsi"><?php echo set_value('deskripsi') ?></textarea>
                        <?php echo form_error('deskripsi') ?>
                    </div>
                </div>


                (Check List ini diisi pemohon, semata-mata dimaksudkan untuk mengetahui pemenuhan kelengkapan surat permohonan dan TIDAK dimaksudkan untuk MENGEVALUASI surat permohonan/proposal)
                <table class="table " id="barang_info_table">
                    <thead>
                        <tr>
                            <th style="width:10%">No</th>
                            <th style="width:85%">Kelengkapan Dokumen</th>
                            <th style="width:5%"></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php 
                            $x = 0;
                            foreach ($row->result() as $key => $data) { 
                            $x++;
                        ?>
                        <tr>
                            <td>
                                <input type="hidden" name="id_ceklis[]" value="<?php echo $data->id ; ?>" >
                                <?php echo $data->nomor ; ?>
                            </td>
                            <td>
                                <?php echo $data->dokumen ; ?>
                            </td>
                            <td>
                                <div class="checkbox checkbox-css">
                                    <input type="checkbox" id="nf_checkbox_css_<?php echo $x; ?>" name="ceklis_pemohon[]" value="1">
                                    <label for="nf_checkbox_css_<?php echo $x; ?>"></label>
                                </div>
                            </td>
                        </tr>
                        <?php 
                            }
                        ?>
                    </tbody>
                </table>
            

                <div class="panel-footer text-right">
                    <a href="<?php echo base_url('user'); ?>"  class="btn btn-white btn-sm">Batal</a>
                    <button type="submit" name="<?php echo $page ?>" class="btn btn-primary btn-sm m-l-5">Simpan</button>
                </div>

            <?php echo form_close() ?>
        </div>
        <!-- end panel-body -->
    </div>
    <!-- end panel -->
</div>
<!-- end #content -->

