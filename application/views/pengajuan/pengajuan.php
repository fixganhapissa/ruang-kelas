<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="<?php echo base_url('user') ?>">Pengajuan</a></li>
		<li class="breadcrumb-item active">Data</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Pengajuan<small></small></h1>
	<!-- end page-header -->
	
    <?php $this->view('message') ?>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title">
				<a href="<?php echo base_url('pengajuan/add'); ?>">
					<span class="label label-success pull-left m-r-10">Add</span>
				</a> Data Pengajuan
			</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->

		<div class="panel-body">
			<table id="data-table-responsive" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>No</th>
						<th>Judul</th>
						<th>Tanggal Pengajuan</th>
						<th>Proposal</th>
						<th>Deskripsi</th>
						<th class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($row->result() as $key => $data) {
					# code...
				?>
					<tr class="odd gradeX">
						<td><?php echo $no++ ; ?></td>
						<td><?php echo $data->judul ; ?></td>
						<td><?php echo $data->tanggal_pengajuan ; ?></td>
						<td><?php echo $data->proposal ; ?></td>
						<td><?php echo $data->deskripsi ; ?></td>
						<td class="text-center">

							<a href="<?php echo base_url('pengajuan/update/'.$data->id_pengajuan); ?>">
								<button class="btn btn-space btn-primary btn-sm">Update</button>
							</a>
							
							<a href="<?php echo base_url('pengajuan/del/'.$data->id_pengajuan); ?>"  onclick="return confirm('Apakah Anda Yakin?')">
                                <button class="btn btn-space btn-danger btn-sm">Delete</button>
                            </a>
						</td>	
					</tr> 
				<?php 
				}
				?>
				</tbody>
			</table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
</div>
<!-- end #content -->



